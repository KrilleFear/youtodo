import QtQuick 2.9
import QtTest 1.2
import QtQuick.LocalStorage 2.0
import "../src/YouTodo.js" as YouTodo

TestCase {
    id: testCase
    name: "Tasks tests"

    function test_create() {
        var tasks = new YouTodo.Tasks(testCase);
        compare(tasks.listModel !== undefined, true, "listModel should exist");
        compare(tasks.store !== undefined, true, "store should exist");
    }

    function test_addTask() {
        var tasks = new YouTodo.Tasks(testCase);
        tasks.store.transaction("DELETE FROM Tasks");
        tasks = new YouTodo.Tasks(testCase);
        tasks.addTask("test task");
        compare(tasks.listModel.count, 1, "tasks count should be 1");
    }

    function test_cleanUp() {
        var tasks = new YouTodo.Tasks(testCase);
        tasks.store.transaction("DELETE FROM Tasks");
        tasks = new YouTodo.Tasks(testCase);
        tasks.addTask("test task");
        var task = new YouTodo.Task(tasks.listModel.get(0).title, tasks.listModel.get(0).done, tasks, 0);
        task.toggle();
        tasks.cleanUp();
        compare(tasks.listModel.count, 0, "tasks count should be 0");
    }

    function test_exists() {
        var tasks = new YouTodo.Tasks(testCase);
        tasks.addTask("test task");
        compare(tasks.exists("test task"), true, 'tasks.exists should find test task');
    }
}