import QtQuick 2.9
import QtTest 1.2
import QtQuick.LocalStorage 2.0
import "../src/YouTodo.js" as YouTodo

TestCase {
    id: testCase
    name: "Task tests"

    function test_create() {
        var task = new YouTodo.Task("Test title", true);
        compare(task.title, "Test title", "Title should be correct");
        compare(task.done, true, "Done should be correct");
        compare(task.tasks, undefined, "Tasks should be null");
        compare(task.index, undefined, "Index should be undefined");
    }

    function test_toggle() {
        var tasks = new YouTodo.Tasks(testCase);
        var task = new YouTodo.Task("Test title", true, tasks, 0);
        tasks.addTask(task.title, task.done);
        task.toggle();
        compare(task.done, false, "Task done should be toggled");
    }

    function test_remove() {
        var tasks = new YouTodo.Tasks(testCase);
        var task = new YouTodo.Task("Test title", true, tasks, 0);
        tasks.addTask(task.title, task.done);
        task.remove();
        compare(tasks.listModel.count, 0, "Task should be removed");
    }
}