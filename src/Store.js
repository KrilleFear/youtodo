// Depends on QtQuick.LocalStorage 2.0
Qt.include("Task.js");

function Store(tasks) {

    this.db = LocalStorage.openDatabaseSync("YouTodoTasks", "1.0", "The Example QML SQL!", 1000000);

    this.transaction = function (query, params) {
        try {
            var result = {};
            this.db.transaction(
                function (tx) {
                    if (params) result = tx.executeSql(query, params);
                    else result = tx.executeSql(query);
                }
            )
            return result;
        }
        catch (e) {
            console.log(e);
        }
    }

    // Load tasks from database
    this.transaction('CREATE TABLE IF NOT EXISTS Tasks(description TEXT, done BOOL)');
    var result = this.transaction('SELECT * FROM Tasks');
    for (var i = 0; i < result.rows.length; i++) {
        var task = new Task(result.rows[i].description, result.rows[i].done);
        tasks.listModel.append(task);
    }

    this.addTask = function (title, done) {
        this.transaction('INSERT INTO Tasks VALUES(?, ?)', [title, done]);
    }

    this.removeTask = function (title) {
        this.transaction('DELETE FROM Tasks WHERE description=?', [title]);
    }

    this.updateTaskStatus = function (title, done) {
        this.transaction('UPDATE Tasks SET done=? WHERE description=?', [done, title]);
    }

    this.clearDoneTasks = function () {
        this.transaction('DELETE FROM Tasks WHERE done = 1');
    }
}