// Depends on QtQuick.LocalStorage 2.0
Qt.include("Task.js");
Qt.include("Store.js");

function Tasks(appComponent) {

    this.listModel = Qt.createQmlObject('import QtQuick 2.9; ListModel {}', appComponent);
    this.store = new Store(this);

    this.addTask = function (title) {
        var newTask = new Task(title, false);
        this.store.addTask(title, false);
        this.listModel.append(newTask);
    }

    this.cleanUp = function () {
        for (var i = 0; i < this.listModel.count; i++) {
            if (this.listModel.get(i).done) {
                this.listModel.remove(i);
                i--;
            }
        }
        this.store.clearDoneTasks();
    }

    this.exists = function (title) {
        for (var i = 0; i < this.listModel.count; i++) {
            if (this.listModel.get(i).title == title) {
                return true;
            }
        }
        return false;
    }
}