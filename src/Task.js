function Task(title, done, tasks, index) {

    this.title = title;
    this.done = done ? true : false;
    this.tasks = tasks;
    if (index) this.index = index;

    this.toggle = function () {
        this.done = !this.done;
        this.tasks.store.updateTaskStatus(this.title, this.done);
        this.tasks.listModel.set(this.index, this);
    }

    this.remove = function () {
        this.tasks.store.removeTask(this.title);
        this.tasks.listModel.remove(this.index);
    }

}