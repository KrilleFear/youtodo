import QtQuick 2.4
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3
import "../../YouTodo.js" as YouTodo

ListItem {
    id: taskListItem

    property var task: new YouTodo.Task(title, done, tasks, index)

    onClicked: task.toggle()

    ListItemLayout {
        id: layout
        title.text: task.title || ""
        CheckBox {
            id: checkBox
            SlotsLayout.position: SlotsLayout.Leading
            checked: task.done || false
        }

    }

    leadingActions: ListItemActions {
        actions: [
        Action {
            iconName: "delete"
            onTriggered: task.remove()
        }
        ]
    }
}
